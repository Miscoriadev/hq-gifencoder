async function substractionGifEncode(frameFileList, outputFile, width, height, repeat = -1, quality = 15 ){
    let frames = frameFileList.length;

    let lastFrame = null;

    // Create GIF
    const encoder = new GIFEncoder(width, height);
    let writeStream = fs.createWriteStream(outputFile);
    encoder.createReadStream().pipe(writeStream);
    encoder.start();
    encoder.setRepeat(repeat);
    encoder.setQuality((typeof quality !== 'undefined' && quality)?quality:10);
    encoder.setDispose(1); // Leave old frames in the background (No frame disposing)
    //encoder.setTransparent(0xFF0000);

    for (let frameIndex = 0; frameIndex < frames; frameIndex++){
        let currentFrame = null;

        if (lastFrame == null){
            let data = fs.readFileSync(frameFileList[frameIndex].file);
            currentFrame = PNG.sync.read(data).data;
            lastFrame = currentFrame;
            let transparencyColor = calculateFrameTransparencyColor(currentFrame); // find a color that is not in this image.

            encoder.setDelay(frameFileList[frameIndex].timeout);
            encoder.addFrame(currentFrame);
            encoder.setTransparent(transparencyColor);

            forceGC();

            let used = process.memoryUsage().heapUsed / 1024 / 1024;
            console.log(`After first frame: ${Math.round(used * 100) / 100} MB`);
        } else {
            let data = fs.readFileSync(frameFileList[frameIndex].file);
            let pngFile = PNG.sync.read(data);
            currentFrame = pngFile.data;
            let transparencyColor = calculateFrameTransparencyColor(currentFrame); // find a color that is not in this image.

            let transparancyColorR = (transparencyColor & 0xFF0000) >> 16;
            let transparancyColorG = (transparencyColor & 0x00FF00) >> 8;
            let transparancyColorB = (transparencyColor & 0x0000FF);

            let updatedFrame = [];

            // Cut every pixel from the frame that was already present in the last frame (RGBA)
            for (let pixelIndex = 0; pixelIndex < currentFrame.length/4; pixelIndex++){
                if (lastFrame[pixelIndex*4] == currentFrame[pixelIndex*4] && // R
                    lastFrame[pixelIndex*4 + 1] == currentFrame[pixelIndex*4 +1] && // G
                    lastFrame[pixelIndex*4 + 2] == currentFrame[pixelIndex*4 +2] && // B
                    lastFrame[pixelIndex*4 + 3] == currentFrame[pixelIndex*4 +3]) { // A

                    updatedFrame[pixelIndex*4] = transparancyColorR;
                    updatedFrame[pixelIndex*4+1] = transparancyColorG;
                    updatedFrame[pixelIndex*4+2] = transparancyColorB;
                    updatedFrame[pixelIndex*4+3] = 255;
                } else {
                    updatedFrame[pixelIndex*4] = currentFrame[pixelIndex*4]; // R
                    updatedFrame[pixelIndex*4 + 1] = currentFrame[pixelIndex*4 +1]; // G
                    updatedFrame[pixelIndex*4 + 2] = currentFrame[pixelIndex*4 +2]; // B
                    updatedFrame[pixelIndex*4 + 3] = 255; // A
                }
            }
            encoder.setTransparent(transparencyColor);
            encoder.setDelay(frameFileList[frameIndex].timeout);
            encoder.addFrame(updatedFrame);

            lastFrame = currentFrame;

            forceGC();

            let used = process.memoryUsage().heapUsed / 1024 / 1024;
            console.log(`After subsequent frames: ${Math.round(used * 100) / 100} MB`);
        }
    }

    encoder.finish();

    await new Promise((resolve, reject) => {
        writeStream.on('finish', resolve);
        writeStream.on('error', reject);
    });
}

function calculateFrameTransparencyColor(frameData){
    let colors = [];
    for (let i = 0; i < frameData.length/4; i++){
        let color = (frameData[i] << 16) + (frameData[i+1] << 8) + frameData[i+2]; // change to 3 byte hex (ignore alpha)
        colors[color] = true;
    }

    for (let colorIter = 0; colorIter < 16777215; colorIter++){
        if (typeof colors[colorIter] == "undefined"){
            return colorIter;
        }
    }

    /*let colors = [];
    console.log("Start color indexing");
    for (let i = 0; i < frameData.length/4; i++){
        let color = (frameData[i] << 16) + (frameData[i+1] << 8) + frameData[i+2]; // change to 3 byte hex (ignore alpha)
        if (!colors.includes(color)){
            colors.push(color);
        }
    }
    console.log("Stop color indexing");

    return findMissingPositive(colors, 16777215) // size is the max amount of colors in 3 byte RGB spectrum. Return 3 byte hex color. */
}