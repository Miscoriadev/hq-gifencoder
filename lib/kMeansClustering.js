/*
  Constructor: kMeansClustering

  Arguments:

  pixels - array of pixels in RGB format
  samplefac - sampling factor 1 to 30 where lower is better quality

  >
  > pixels = [r, g, b, r, g, b, r, g, b, ..]
  >
*/
function kMeansClustering(pixels, samplefac, K = 255) {
    let maxIter = 10; // Amount of iterations to recalculate the centroids
    let centroids = [];
    let colorIndex = [];

    /*
      Private Method: init

      sets up arrays
    */
    function init() {

        // Initiate random centroids
        let i = 0;
        let attempts = 0;

        //maxIter = 30 - samplefac;
        while (i < K){
            let centroid = getRandomInt(pixels.length/3);

            let index = centroids.findIndex((element) => {
               return element[0] == pixels[centroid] && element[1] == pixels[centroid+1] && element[2] == pixels[centroid+2];
            });

            if (index == -1){
                centroids.push([pixels[centroid], pixels[centroid+1], pixels[centroid+2]]);
                i++;
                attempts = 0;
            } else {
                attempts++;
            }

            if (attempts > pixels.length/9){
                while (i < K){
                    centroids.push([0, 0, 0]);
                    i++;
                }
                break;
            }
        }
    }

    function initkMeansPlusPlus(){
        console.debug("Initiating K-means++");
        let startTime = new Date().getTime();

        // Initiate centroids
        let i = 0;

        // Pick initial random centroid
        let centroid = getRandomInt(pixels.length/3);
        centroids.push([pixels[centroid], pixels[centroid+1], pixels[centroid+2]]);

        let lowestCentroidDistanceToPixel = [];
        while (i < K){
            let distanceToPixels = [];

            let total = 0;
            for (let p = 0; p < pixels.length/3; p++){ // For every pixel
                let distance = calculateSmallestCentroidDistanceSquared(p, lowestCentroidDistanceToPixel, centroids[i]);
                distanceToPixels.push(distance);

                total += distance;
            }

            let centroid = weightedRand(distanceToPixels, total);
            centroids.push([pixels[centroid*3], pixels[centroid*3+1], pixels[centroid*3+2]]);

            lowestCentroidDistanceToPixel = distanceToPixels;
            i++;
        }

        /*for (let i = 0; i < centroids.length; i++){

        }*/

        console.debug("Done initiating K-means++! It took: %d milliseconds", new Date().getTime() - startTime);
    }

    function calculateSmallestCentroidDistanceSquared(pixel, lowestCentroidDistanceToPixel, newCentroid){
        let smallestDistance = -1;
        if (lowestCentroidDistanceToPixel.length > 0){
            smallestDistance = lowestCentroidDistanceToPixel[pixel];
        }

        let rDistance = Math.pow(pixels[pixel*3] - newCentroid[0],2);
        let gDistance = Math.pow(pixels[pixel*3 + 1] - newCentroid[1],2);
        let bDistance = Math.pow(pixels[pixel*3 + 2] - newCentroid[2],2);

        let distance = rDistance + gDistance + bDistance;

        if (distance < smallestDistance || smallestDistance == -1){
            smallestDistance = distance;
        }

        return smallestDistance;
    }

    function weightedRand(spec, total) {
        let i;
        let sum=0;
        let r=Math.random()*total;

        for (i in spec) {
            sum += spec[i];
            if (r <= sum) return i;
        }
    }

    /*
      Private Method: getRandomInt

      returns a random int
    */
    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    /*
      Private Method: inxbuild

      sorts network and builds netindex[0..255]
    */
    function inxbuild() {

        let startTime = new Date().getTime();
        console.debug("Building index...");

        // Build an empty multidimensional array
        for (let i = 0; i < 256; i++){
            colorIndex[i] = [];
            for (let j = 0; j < 256; j++){
                colorIndex[i][j] = [];
            }
        }

        let idx = findClosestCentroids(centroids);
        for (let i = 0; i < idx.length; i++){
            colorIndex[pixels[i*3]][pixels[i*3+1]][pixels[i*3+2]] = idx[i];
        }

        console.debug("Done building index! It took: %d milliseconds.", new Date().getTime() - startTime);
    }

    /*
      Private Method: inxsearch

      searches for RGB values 0..255 and returns a color index
    */
    function inxsearch(r, g, b) {
        if (colorIndex[r][g][b] !== 'undefined'){
            return colorIndex[r][g][b];
        }
    }

    /*
      Private Method: learn

      "Main Learning Loop"
    */
    function learn() {
        let startTime = new Date().getTime();
        let currentCentroids = centroids;
        let idx = [];

        for (let i = 0; i < maxIter; i++){
            //console.debug("K-means Iteration %d/%d", i, maxIter);

            idx = findClosestCentroids(currentCentroids);
            currentCentroids = computeCentroids(idx);
        }

        centroids = currentCentroids;
        console.debug("Done training! It took: %d milliseconds.", new Date().getTime() - startTime);
    }

    function findClosestCentroids(currentCentroids){
        let idx = [];

        for (let i = 0; i < pixels.length/3; i++){
            let lowestDistance = -1;
            let lowestDistanceCentroid = -1;
            for (let j = 0; j < K; j++){
                let distance = getEucledianDistanceOfColor(i, currentCentroids[j]);

                if ((lowestDistance > distance) | lowestDistance == -1){
                    lowestDistance = distance;
                    lowestDistanceCentroid = j;
                }
            }
            idx[i] = lowestDistanceCentroid;
        }

        return idx;
    }

    function getEucledianDistanceOfColor(pixel, centroid){
        let rDistance = Math.pow(pixels[pixel*3] - centroid[0],2);
        let gDistance = Math.pow(pixels[pixel*3 + 1] - centroid[1],2);
        let bDistance = Math.pow(pixels[pixel*3 + 2] - centroid[2],2);

        return Math.sqrt(rDistance + gDistance + bDistance);
    }

    function computeCentroids(idx){
        let newCentroids = [];

        for (let i = 0; i < K; i++){
            newCentroids[i] = [0,0,0];

            let ni = 0;
            let sumOfR = 0;
            let sumOfG = 0;
            let sumOfB = 0;

            for (let j = 0; j < idx.length; j++){
                if (idx[j] == i){
                    ni++;
                    sumOfR += pixels[j];
                    sumOfG += pixels[j+1];
                    sumOfB += pixels[j+2];
                }
            }

            if (ni > 0) {
                sumOfR = Math.round(sumOfR/ni); // divide by the amount of pixels in the cluster
                sumOfG = Math.round(sumOfG/ni); // divide by the amount of pixels in the cluster
                sumOfB = Math.round(sumOfB/ni); // divide by the amount of pixels in the cluster
                newCentroids[i] = [sumOfR, sumOfG, sumOfB];

            } else {
                newCentroids[i] = centroids[i];
            }

        }

        return newCentroids;
    }

    /*
      Method: buildColormap

      1. initializes network
      2. trains it
      3. removes misconceptions
      4. builds colorindex
    */
    function buildColormap() {
        //init();
        initkMeansPlusPlus();
        //learn();
        inxbuild();
    }
    this.buildColormap = buildColormap;

    /*
      Method: getColormap

      builds colormap from the index

      returns array in the format:

      >
      > [r, g, b, r, g, b, r, g, b, ..]
      >
    */
    function getColormap() {
        let output = [];
        for (let i = 0; i < centroids.length; i++){
            for (let j = 0; j < centroids[i].length; j++){
                output.push(centroids[i][j]);
            }
        }
        return output;
    }
    this.getColormap = getColormap;

    /*
      Method: lookupRGB

      looks for the closest *r*, *g*, *b* color in the map and
      returns its index
    */
    this.lookupRGB = inxsearch;
}

module.exports = kMeansClustering;
